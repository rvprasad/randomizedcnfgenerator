# RandomizedCNFGenerator

A tool to generate CNF formulae using the Mitchell's L-SAT problem generator described in [*Introduction in Evolutionary Algorithms* by *A. E. Eiben* and *J. E. Smith*](http://www.springer.com/us/book/9783642072857).


## Attribution

Copyright (c) 2017, Venkatesh-Prasad Ranganath

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

**Authors:** Venkatesh-Prasad Ranganath
