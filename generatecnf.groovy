/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

final MAX_CLAUSE_LENGTH = '3'
final cli = new CliBuilder(usage:'groovy generatecnf.groovy')
cli.header = 'Tool generates randomized CNF in DIMACS format ' +
        '(http://www.satcompetition.org/2009/format-benchmarks2009.html)'
cli.c(longOpt:'numOfClauses', args:1, argName:'numOfClauses', required:true,
    'Number of clauses to occur in the CNF')
cli.m(longOpt:'maxClauseLength', args:1, argName:'maxClauseLength',
    "Max length of each clause in the CNF. (Default: $MAX_CLAUSE_LENGTH")
cli.v(longOpt:'numOfVars', args:1, argName:'numOfVars', required:true,
    'Number of variables to occur in the CNF.')

final options = cli.parse(args)
if (!options) {
    return
}

final numOfClauses = options.c as int
final clauseLength = (options.m ?: MAX_CLAUSE_LENGTH) as int
final numOfVars = options.v as int
final random = new Random()

println("p cnf $numOfVars $numOfClauses")
(1..numOfClauses).each {
    final tmp1 = (1..clauseLength).collect {
        (random.nextInt(numOfVars) + 1) * (random.nextBoolean() ? 1 : -1)
    } as Set
    println("${tmp1.join(' ')} 0")
}

